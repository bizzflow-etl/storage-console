import json
from time import time

import airflow
from flask import Flask, render_template, request
from flask_login import LoginManager, login_required
from google.cloud import bigquery, storage
from models import db, User

from proxyfix import ReverseProxyPrefixFix

from toolkit import current_config
from toolkit.managers.storage.bq_sql import BqStorageManager
from toolkit.managers.storage.snowflake_sql import SnowflakeStorageManager
from toolkit.managers.storage.azure_sql import AzureSQLStorageManager
from toolkit.base.kex import Kex
from toolkit.base.table import Table

from decimal import Decimal
import datetime

FORMAT_DATETIME = "%Y-%m-%d %H:%M:%S"
FORMAT_DATE = "%Y-%m-%d"
FORMAT_TIME = "%H:%M:%S"

app = Flask(__name__)

app.debug = True
app.config.from_mapping(
    SQLALCHEMY_DATABASE_URI=airflow.conf.get('core', 'sql_alchemy_conn'),
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
    SECRET_KEY=airflow.conf.get('webserver', 'secret_key'),
    REVERSE_PROXY_PATH="/storage"
)
db.init_app(app)

login_manager = LoginManager(app)
login_manager.login_view = "/"

ReverseProxyPrefixFix(app)

tree = {}
preview = {}
data_caching = 3600

StorageManager = current_config.get_class("storage_manager")


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


def get_storage_manager():
    if current_config.classes["storage_manager"] == "BqStorageManager":
        bq_client = bigquery.Client()
        gcs_client = storage.Client()
        return BqStorageManager(
            bq_client=bq_client,
            gcs_client=gcs_client
        )
    elif current_config.classes["storage_manager"] == "SnowflakeStorageManager":
        return SnowflakeStorageManager()
    elif current_config.classes["storage_manager"] == "AzureSQLStorageManager":
        return AzureSQLStorageManager()


@app.route("/")
@login_required
def index():
    return render_template("tree.html", tree=tree, tree_json=json.dumps(tree))


def json_output_encode(data):
    if isinstance(data, Decimal):
        return float(data)
    if isinstance(data, datetime.time):
        return data.strftime(FORMAT_TIME)
    if isinstance(data, datetime.date):
        return data.strftime(FORMAT_DATE)
    if isinstance(data, datetime.datetime):
        return data.strftime(FORMAT_DATETIME)


@app.route("/kex/<kex>/tables/<table>/preview")
@login_required
def table_preview(kex, table):
    storage_manager = get_storage_manager()
    global preview, data_caching
    t = Table(kex, table)
    tid = t.get_id()
    if tid in preview:
        if (time() - preview[tid]["last_update"]) < data_caching:
            print("Returing cached")
            return {"rows": preview[tid]["rows"]}

    p = storage_manager.preview(t, number_results=10)

    preview[tid] = {}
    preview[tid]["rows"] = p["rows"]
    preview[tid]["last_update"] = time()

    return json.dumps(p, default=json_output_encode), 200, {"Content-Type": "application/json"}


@app.before_first_request
@app.route("/kex/")
@login_required
def get_kex():
    storage_manager = get_storage_manager()
    global tree
    tree = {}
    for kex in storage_manager.list_kexes():
        tree[kex.kex] = {
            "tables": {}
        }
    return tree


@app.route("/kex/<kex>/tables")
@login_required
def get_tables(kex):
    storage_manager = get_storage_manager()
    if (request.args.get("force", None) == "true"):
        force = True
    else:
        force = False
    global tree
    if kex in tree and "last_update" in tree[kex]:
        if (time() - tree[kex]["last_update"] < data_caching) and not force:
            print("Returing cached")
            return tree

    tables = storage_manager.list_tables(Kex(project=None, kex=kex))
    tree[kex]["tables"] = {}

    for table in tables:
        storage_manager.get_table_details(table)
        tree[kex]["tables"][table.table] = table.to_dict()
    tree[kex]["last_update"] = time()
    return tree


@app.route("/kex/<kex>/tables/<table>", methods=["DELETE"])
@login_required
def delete_table(kex, table):
    storage_manager = get_storage_manager()
    k = Kex(project=None, kex=kex)
    t = Table(k, table, None)

    storage_manager.delete_table(t)
    return {
        "id": table
    }


@app.route("/kex/<kex>/tables/<table>/truncate", methods=["POST"])
@login_required
def truncate_table(kex, table):
    storage_manager = get_storage_manager()
    k = Kex(project=None, kex=kex)
    t = Table(k, table)

    storage_manager.truncate_table(t)
    return {
        "id": table
    }


@app.route("/kex/<kex>", methods=["POST"])
@login_required
def create_kex(kex):
    global tree
    storage_manager = get_storage_manager()
    k = Kex(project=None, kex=kex)

    storage_manager.create_kex(k)
    return {
        "id": kex
    }


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8082)
