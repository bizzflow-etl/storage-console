ARG TOOLKIT_TAG=latest
FROM registry.gitlab.com/bizzflow-etl/toolkit:${TOOLKIT_TAG}

USER root
RUN pip install uwsgi

USER bizzflow

WORKDIR /storage-console

COPY . ./

EXPOSE 8082

ARG VERSION="unknown"
ENV VERSION=${VERSION}
ARG GIT_SHA="unknown"
ENV GIT_SHA=${GIT_SHA}

LABEL app.bizzflow_storage_console.name="Bizzflow Storage Console" \
    app.bizzflow_storage_console.author="Bizztreat <info@bizztreat.com>" \
    app.bizzflow_storage_console.version="${VERSION}" \
    app.bizzflow_storage_console.git_sha="${GIT_SHA}" \
    app.bizzflow_storage_console.port="8082"

CMD [ "uwsgi", "--http", "0.0.0.0:8082", "--master", "--module", "main:app", "--processes", "1" ]
