# TreeView

## Running
Requirements: Bizzflow R2 toolkit

### UNIX-like

*Make sure you are running python3 (`python3` instead of `python` may be neccessary)*

```bash
git clone git@gitlab.com:bizztreat/dev/tree-view.git
cd tree-view
python -m venv venv
. venv/bin/activate
python -m pip install -r requirements.txt
cd ..
git clone git@gitlab.com:bizztreat/bizzflow/r2/toolkit.git
cd toolkit
python -m pip install --editable .
cd ../tree-view
export FLASK_APP="main.py"
export FLASK_ENV="development"
export GOOGLE_APPLICATION_CREDENTIALS="<absolute-path-to-service-account.json>"
flask run --port 9999
```

Visit [http://localhost:9999/](http://localhost:9999)

### Windows

```powershell
git clone git@gitlab.com:bizztreat/dev/tree-view.git
cd tree-view
python -m venv venv
.\venv\Scripts\activate
python -m pip install -r requirements.txt
cd ..
git clone git@gitlab.com:bizztreat/bizzflow/r2/toolkit.git
cd toolkit
python -m pip install --editable .
cd ../tree-view
$env:FLASK_APP="main.py"
$env:FLASK_ENV="development"
$env:GOOGLE_APPLICATION_CREDENTIALS="<absolute-path-to-service-account.json>"
flask run --port 9999
```

Visit [http://localhost:9999/](http://localhost:9999)

## Known issues
Error handling for creating kex with spaces in the kex name is missing
